# docker_node_plus

This repo creates an optimized docker image based off of [node](https://hub.docker.com/_/node) with some build tools.

## How to publish a new image?

1. If more than the Node version has changed in the `Dockerfile`, then bump the `VERSION`.

2. Login to GitLab docker registry.

```shell
docker login registry.gitlab.com
```

3. Build docker image locally and tag with registry URL.

```shell
docker build -t registry.gitlab.com/gitlab-org/frontend/playground/docker_node_plus .
```

4. Locally tag `:latest` with `:<NODE_VERSION>-<REPOSITORY_VERSION>` where `<NODE_VERSION>` is the
   version of the node image used in the `Dockerfile` and `<REPOSITORY_VERSION>` is the version in
   this repository's `VERSION` file. Example:

```shell
# JUST AN EXAMPLE! DO NOT USE WITHOUT CHANGING `<NODE_VERSION>-<REPOSITORY_VERSION>`!
docker tag registry.gitlab.com/gitlab-org/frontend/playground/docker_node_plus:latest registry.gitlab.com/gitlab-org/frontend/playground/docker_node_plus:<NODE_VERSION>-<REPOSITORY_VERSION>
```

5. Push new image with the new tag used in the previous step.

```shell
# JUST AN EXAMPLE! DO NOT USE WITHOUT CHANGING `<NODE_VERSION>-<REPOSITORY_VERSION>`!
docker push registry.gitlab.com/gitlab-org/frontend/playground/docker_node_plus:<NODE_VERSION>-<REPOSITORY_VERSION>
```

## How is this tested?

1. Clone the repo that needs the image https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode-prod locally somewhere

```shell
cd $HOME/dev/projects/temp
git clone git@gitlab.com:gitlab-org/frontend/playground/gitlab-vscode-prod.git
```

2. Build docker image locally. Remember the image sha that is created!

```shell
docker build .
```

3. Start container based on new image and mount volume pointing to locally cloned repo.

```shell
docker run --rm -it -v $HOME/dev/projects/temp:/home/node/temp <IMAGE_SHA> /bin/sh
```

4. From the container, try to build the project

```shell
cd /home/node/temp/gitlab-vscode-prod
yarn install && yarn run build:example
```
